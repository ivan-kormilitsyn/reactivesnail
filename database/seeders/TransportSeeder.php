<?php

namespace Database\Seeders;

use App\Enum\TransportStatus;
use App\Models\Transport;
use App\Models\TransportTypes;
use App\Models\User;
use Illuminate\Database\Seeder;


class TransportSeeder extends Seeder
{
    protected $transports = [
        [
            'gos_number' => 'eu123m',
            'year_of_issue' => '2010',
            'name' => 'Иван Петрович',
            'type' => 'грузовики',
            'status' => TransportStatus::ON_REPAIR,
        ],
        [
            'gos_number' => 'h432mk',
            'year_of_issue' => '2013',
            'name' => 'Василий Иванович',
            'type' => 'фургоны',
            'status' => TransportStatus::NOT_USED,
        ],
        [
            'gos_number' => '200ry',
            'year_of_issue' => '2006',
            'name' => 'Василий Иванович',
            'type' => 'спецтехника',
            'status' => TransportStatus::SOLD,
        ],
        [
            'gos_number' => 'm999n',
            'year_of_issue' => '2020',
            'name' => 'Алексей Николаевич',
            'type' => 'легковые-авто',
            'status' => TransportStatus::ACTIVE,
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->transports as $value) {

            $value['type_id'] = TransportTypes::where('type', $value['type'])->firstOrFail()->getKey();
            unset($value['type']);

            $value['user_id'] = User::where('name', $value['name'])->firstOrFail()->getKey();
            unset($value['name']);

            Transport::create($value);
        }

    }
}
