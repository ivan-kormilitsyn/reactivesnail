<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Database\Seeder;


class UsersSeeder extends Seeder
{
    protected $users = [
        [
            'name' => 'Василий Иванович',
            'email' => 'vasiliy@mail.ru',
            'password' => 'ctrl+alt+del',
        ],
        [
            'name' => 'Иван Петрович',
            'email' => 'petrovich@gmail.ru',
            'password' => 'latePassword',
        ],
        [
            'name' => 'Алексей Николаевич',
            'email' => 'alnick@gmail.ru',
            'password' => 'newPassword',
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->users as $value) {
            User::create($value);
        }

    }
}
