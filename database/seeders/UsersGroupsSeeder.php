<?php

namespace Database\Seeders;

use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Database\Seeder;


class UsersGroupsSeeder extends Seeder
{
    protected $usersGroupSeeder = [
        [
            'group_name' => 'Менеджер',
        ],
        [
            'group_name' => 'Водитель',
        ],
        [
            'group_name' => 'Администратор',
        ],
    ];


    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->usersGroupSeeder as $value) {
            UserGroup::create($value);
        }

    }
}
