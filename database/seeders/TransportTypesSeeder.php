<?php

namespace Database\Seeders;

use App\Models\TransportTypes;
use Illuminate\Database\Seeder;


class TransportTypesSeeder extends Seeder
{
    protected $transportsTypes = [
        [
            'type' => 'легковые-авто'
        ],
        [
            'type' => 'грузовики'
        ],
        [
            'type' => 'фургоны'
        ],
        [
            'type' => 'спецтехника'
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->transportsTypes as $value) {
            TransportTypes::create($value);
        }

    }
}
