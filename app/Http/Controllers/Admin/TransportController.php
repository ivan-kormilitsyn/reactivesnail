<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\TypesResource;
use App\Models\Transport;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use function Termwind\render;

class TransportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     * \Illuminate\Database\Eloquent\Collection
     */
    public function index()
    {

//        $transportWithTypesAndUsers = Transport::with('type', 'user')->get();
           $transport = Transport::with('type', 'user')->get();

        return view('');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Builder[]|Collection
     */
    public function create()
    {
        return Transport::with('type', 'user')->get();

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transportFromClient = $request->all();

        DB::table('transport')->insert($transportFromClient);

        return response('OK', 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $transport = $request->only(['gos_number', 'user_id', 'status']);

        DB::table('transport')->where('id', $id)->update($transport);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Transport $transport)
    {
        $transport->delete();
    }
}
