<?php

namespace App\Enum;

enum TransportStatus: string
{
    case ACTIVE = 'active';
    case ON_REPAIR = 'on_repair';
    case SOLD = 'sold';
    case NOT_USED = 'not_used';

    public static function getStatusValue(): array
    {
        $values = [];
        foreach (self::cases() as $case) {
            $values[] = $case->value;
        }

        return $values;
    }
}
