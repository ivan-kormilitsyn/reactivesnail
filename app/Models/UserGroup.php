<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{

    public $timestamps = false;

    public function user()
    {
        return $this->belongsToMany(
            User::class,
            'user_has_group',
            'group_id',
            'user_group_id'
        );
    }

}
