<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportTypes extends Model
{

    protected $fillable = [
        'type',
    ];
    public $timestamps = false;

    protected $primaryKey = 'type_id';

    public function transport()
    {
        return $this->belongsTo(Transport::class, 'type_id', 'type_id');
    }

}
