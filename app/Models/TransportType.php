<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransportType extends Model
{

    protected $fillable = [
        'type',
    ];

    protected $primaryKey = 'type_id';

        public function transport()
        {
            return $this->belongsTo(Transport::class);
        }

}
