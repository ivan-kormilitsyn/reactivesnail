<?php

namespace App\Models;

use App\Enum\TransportStatus;
use Illuminate\Database\Eloquent\Model;

class Transport extends Model
{
    protected $table = 'transport';

    protected $fillable = [
        'gos_number',
        'year_of_issue',
        'user_id',
        'type_id',
        'status',
    ];

    public $timestamps = false;

    protected $casts = [
        'status' => TransportStatus::class
    ];

    public function type()
    {
        return $this->hasOne(TransportTypes::class, 'type_id', 'type_id');
    }
    public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }


}
